#!/usr/bin/env bash

LOCAL_FILES=tmp/*.csv
for file in $LOCAL_FILES
do 
	echo "Processing $file"
	/snap/bin/docker exec hannah_web_1 python manage.py import_transactions $file
done

echo "Cleaning up. Removing tmp directory."
rm -rf tmp/
