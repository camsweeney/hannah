import logging
from django.shortcuts import render
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import FormView
from django.forms import modelformset_factory
from django.http import HttpResponseRedirect
from django.db.models import Sum

from budget.models import Transaction, Category, CategoryPlannedTotal
from budget.forms import ExpenseTransactionForm, \
    IncomeTransactionForm, TransactionFilterForm, TransactionFormSetHelper

logger = logging.getLogger(__name__)


class TransactionSelectView(LoginRequiredMixin, FormView):
    template_name = 'transactions_select.html'
    form_class = TransactionFilterForm
    login_url = 'accounts/login/'
    redirect_field_name = 'next'

    def get_context_data(self, request, *args, **kwargs):
        context = super().get_context_data()
        context['expense_form'] = TransactionFilterForm()
        context['income_form'] = TransactionFilterForm()

        return context

    def get(self, request):
        return self.render_to_response(self.get_context_data(request))


class TransactionView(LoginRequiredMixin, FormView):
    template_name = 'transactions.html'

    def post(self, request, *args, **kwargs):
        TransactionFormSet = modelformset_factory(Transaction,
                                                  form=self.form,
                                                  exclude=('is_debit',),
                                                  max_num=10, extra=0)
        formset = TransactionFormSet(request.POST)
        if formset.is_valid():
            logger.info("Form saved")
            formset.save()
            return HttpResponseRedirect('/')

        print(formset.errors)

        return render(request, self.template_name,
                      context=self.get_context_data())

    def filter_transactions(self, **kwargs):
        now = timezone.now()
        month = kwargs.get('month', now.month)
        year = kwargs.get('year', now.year)
        is_debit = kwargs.get('is_debit')

        qset = Transaction.objects.filter(
            date__month=month, date__year=year,
            is_debit=is_debit).order_by('date')

        return qset

    def get_context_data(self, **kwargs):
        TransactionFormSet = modelformset_factory(
            Transaction, form=self.form, exclude=('is_debit',),
            max_num=10, extra=0)

        queryset = self.filter_transactions(**kwargs)
        formset = TransactionFormSet(queryset=queryset)

        helper = TransactionFormSetHelper()

        choices = {}

        context = dict()
        context['helper'] = helper
        context['formset'] = formset
        context['choices'] = choices

        return context


class ExpenseView(TransactionView):
    form = ExpenseTransactionForm

    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(
            month=request.GET.get('month'),
            year=request.GET.get('year'),
            category=request.GET.get('category__isnull'),
            is_debit=True
        )
        return render(request, self.template_name, context=context)

    def filter_transactions(self, **kwargs):
        qset = super().filter_transactions(**kwargs)

        category = kwargs.get('category')
        if category:
            qset = qset.filter(category__isnull=category)

        return qset


class IncomeView(TransactionView):
    form = IncomeTransactionForm

    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(
            month=request.GET.get('month'),
            year=request.GET.get('year'),
            category=request.GET.get('category__isnull'),
            is_debit=False
        )
        return render(request, self.template_name, context=context)


class Row(object):
    """
    A row in the dashboard table consisting of the planned category amount,
    the actual calculated amount, the different between them and the
    category label.
    """
    planned_amount = 0
    actual_amount = 0
    diff = 0
    label = None


class DashboardView(LoginRequiredMixin, FormView):
    template_name = 'dashboard.html'
    form_class = TransactionFilterForm

    def get_actual_total(self, is_debit, month, year):
        total = Transaction.objects.filter(
                date__month=month,
                date__year=year,
                category__isnull=False,
                is_debit=is_debit
            ).aggregate(Sum('amount'))

        return total['amount__sum'] if total['amount__sum'] else 0

    def get_planned_total(self, is_debit=True):
        total = CategoryPlannedTotal.objects.filter(
            is_debit=is_debit).aggregate(Sum('amount'))

        return total['amount__sum'] if total['amount__sum'] else 0

    def get_category_total(self, is_debit, month, year):
        """
        A list of dictionaries with category label, actual sum,
        planned sum and the difference.
        """
        category_row_list = []
        categories = Category.objects.filter(is_debit=is_debit)

        for category in categories:
            category_sum = Transaction.objects.filter(
                is_debit=is_debit,
                date__month=month,
                date__year=year,
                category=category
            ).order_by('date').aggregate(Sum('amount'))

            try:
                planned_sum = \
                    CategoryPlannedTotal.objects.get(category=category)
            except ObjectDoesNotExist as e:
                continue

            row = Row()
            row.label = category.name
            row.planned_amount = planned_sum.amount

            actual_amount = category_sum['amount__sum']
            row.actual_amount = actual_amount if actual_amount else 0
            row.diff = row.planned_amount - row.actual_amount

            category_row_list.append(row)

        return category_row_list

    def get_category_expense_total(self, month, year):
        return self.get_category_total(True, month, year)

    def get_category_income_total(self, month, year):
        return self.get_category_total(False, month, year)

    def get_context_data(self, request, *args, **kwargs):
        context = super().get_context_data()
        now = timezone.now()
        month = request.GET.get('month', now.month)
        year = request.GET.get('year', now.year)

        # Fetch category sums
        expense_category_sums = \
            self.get_category_expense_total(month, year)
        income_category_sums = \
            self.get_category_income_total(month, year)

        # Fetch transaction actual totals
        total_expense_actual = self.get_actual_total(True, month, year)
        total_income_actual = self.get_actual_total(False, month, year)

        # Fetch planned budget goal totals
        planned_expense = self.get_planned_total(is_debit=True)
        planned_income = self.get_planned_total(is_debit=False)

        # Calculate the total differences
        if total_income_actual and planned_income:
            total_income_diff = total_income_actual - planned_income
        else:
            total_income_diff = 0

        if total_expense_actual and planned_expense:
            total_expense_diff = planned_expense - total_expense_actual
        else:
            total_expense_diff = 0

        # Build the totals dictionary
        totals = {
            'total_expense_actual': total_expense_actual,
            'total_income_actual': total_income_actual,
            'total_expense_planned': planned_expense,
            'total_income_planned': planned_income,
            'total_expense_diff': total_expense_diff,
            'total_income_diff': total_income_diff,
        }

        # Chart calculation
        totals['expense_chart'] = self.calculate_chart_percentage(
            totals['total_expense_planned'], totals['total_expense_actual']
        )
        totals['income_chart'] = self.calculate_chart_percentage(
            totals['total_income_planned'], totals['total_income_actual']
        )

        context['totals'] = totals
        context['expense_transactions'] = expense_category_sums
        context['income_transactions'] = income_category_sums
        return context

    def calculate_chart_percentage(self, planned, actual):
        if planned is None or actual is None:
            return

        if actual > planned:
            percentage = {
                'planned': '{}%'.format(round((planned/actual) * 100)),
                'actual': '100%'
            }
        elif planned > actual:
            percentage = {
                'planned': '100%',
                'actual': '{}%'.format(round((actual/planned) * 100))
            }
        else:
            percentage = {
                'planned': '100%',
                'actual': '100%'
            }

        return percentage

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name,
                      context=self.get_context_data(request))
