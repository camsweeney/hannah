from django.test import TransactionTestCase
from collections import OrderedDict
from budget.utils import parse_csv_transaction, is_duplicate_transaction
from budget.models import Transaction
from datetime import datetime


class TestParseCSVTransactions(TransactionTestCase):

    def setUp(self):
        self.cibc_data = OrderedDict(
            [('date', '2019-04-17'),
             ('description', 'Amazon.ca Prime Member amazon.ca/pri, BC'),
             ('amount', '88.48'), ('paid', ''),
             ('card_number', '4503********9187')]
        )
        self.prospera_visa_data = OrderedDict(
            [('date', '2019-02-17'),
             ('description', 'Amazon.ca Prime Membership signup'),
             ('category', 'Some category'), ('reference_number', '1274936'),
             ('amount', '88.48')]
        )
        self.prospera_data = OrderedDict(
            [('number', '290578379890'),
             ('date', '2019-04-12'),
             ('description', 'Some descriptive description.'),
             ('empty', ''), ('amount', '38.58'), ('deposit', ''),
             ('balance', '5798')]
        )

    # Test CIBC Visa CSV format
    def test_parse_csv_transaction_cibc(self):
        transaction = parse_csv_transaction(self.cibc_data)

        self.assertTrue(transaction.is_debit)

    def test_parse_csv_transaction_cibc_paid(self):
        data = self.cibc_data
        data['paid'] = '80.00'
        data['amount'] = ''

        transaction = parse_csv_transaction(data)

        self.assertFalse(transaction)

    def test_parse_csv_transaction_cibc_invalid_date(self):
        data = self.cibc_data
        data['date'] = 'Fake date 2019'
        transaction = parse_csv_transaction(data)

        self.assertFalse(transaction)

    # Test Prospera Collabria Visa CSV format
    def test_parse_csv_transaction_prospera_visa_debit(self):
        data = self.prospera_visa_data
        transaction = parse_csv_transaction(data)

        self.assertTrue(transaction.is_debit)

    def test_parse_csv_transaction_prospera_visa_negative_removed(self):
        data = self.prospera_visa_data
        data['amount'] = '-18.54'
        transaction = parse_csv_transaction(data)

        self.assertFalse('-' in transaction.amount)

    # Test regular Prospera account CSV format
    def test_parse_csv_transaction_prospera_debit(self):
        data = self.prospera_data
        transaction = parse_csv_transaction(data)

        self.assertTrue(transaction.is_debit)

    def test_parse_csv_transaction_prospera_deposit(self):
        data = self.prospera_data
        data['deposit'] = '300.00'
        data['amount'] = ''
        transaction = parse_csv_transaction(data)

        self.assertFalse(transaction.is_debit)


class TestIsDuplicateTransaction(TransactionTestCase):
    def setUp(self):
        self.debit_transaction = Transaction()
        self.debit_transaction.date = datetime(2019, 5, 24)
        self.debit_transaction.description = 'Some descriptive text'
        self.debit_transaction.amount = 330.45
        self.debit_transaction.is_debit = True

    def test_is_duplicate_transaction_not_duplicate(self):
        self.assertFalse(is_duplicate_transaction(self.debit_transaction))

    def test_is_duplicate_transaction_duplicate(self):
        # Save the existing transaction
        self.debit_transaction.save()

        # Check that transaction with identical attributes is not saved.
        self.assertTrue(is_duplicate_transaction(self.debit_transaction))


class TestImportStatementTransactions(TransactionTestCase):
    pass
