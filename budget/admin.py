from django.contrib import admin
from budget.models import Category, Transaction, CategoryPlannedTotal


class CategoryPlannedTotalAdmin(admin.ModelAdmin):
    list_display = ('category', 'amount', 'is_debit')


class TransactionAdmin(admin.ModelAdmin):
    list_display = ('amount', 'date', 'description', 'category', 'is_debit')
    list_filter = ('is_debit', 'date', 'category', 'is_debit')


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'is_debit')
    list_filter = ('is_debit', )


# Register your models here.
admin.site.register(Transaction, TransactionAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(CategoryPlannedTotal, CategoryPlannedTotalAdmin)
