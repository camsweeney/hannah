from django.db import models
from django.urls import reverse


class Transaction(models.Model):
    date = models.DateField()
    amount = models.DecimalField(decimal_places=2, max_digits=10)
    description = models.CharField(max_length=200)
    category = models.ForeignKey('Category', null=True, blank=True,
                                 on_delete=models.CASCADE)
    is_debit = models.BooleanField(null=True)

    def __str__(self):
        return '{}-{}'.format(self.id, self.description)

    def get_absolute_url(self):
        return reverse('expense_transactions_view')


class Category(models.Model):
    name = models.CharField(max_length=50)
    is_debit = models.BooleanField(null=True)

    def __str__(self):
        return '{}'.format(self.name)

    class Meta:
        verbose_name_plural = 'Categories'


class CategoryPlannedTotal(models.Model):
    amount = models.DecimalField(decimal_places=2, max_digits=10)
    is_debit = models.BooleanField(null=True)
    category = models.ForeignKey('Category', null=True, blank=True,
                                 on_delete=models.CASCADE)

    def __str__(self):
        return 'Planned {} Total'.format(self.category)
