from django.core.management.base import BaseCommand
from budget.utils import import_statement_transactions


class Command(BaseCommand):
    help = 'Import CSV transactions into database.'

    def add_arguments(self, parser):
        parser.add_argument('filename')

    def handle(self, *args, **options):
        import_statement_transactions(options.get('filename'))
