# Generated by Django 2.1.7 on 2019-03-02 20:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('budget', '0003_auto_20190225_2212'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='transaction',
            name='transaction_type',
        ),
        migrations.AddField(
            model_name='transaction',
            name='is_debit',
            field=models.BooleanField(null=True),
        ),
    ]
