from budget.models import Transaction
import csv
from dateutil.parser import parse
from django.core.exceptions import ValidationError
import logging

logger = logging.getLogger(__name__)


def parse_csv_transaction(csv_transaction):
    """
    Parse a row from CSV file to a Transaction object.
    """
    try:
        transaction_date = parse(csv_transaction['date']).date()
    except ValueError as e:
        logger.warning("An error occurred. The message was {}".format(e))
        return

    transaction = Transaction()
    transaction.date = transaction_date
    transaction.description = \
        ' '.join(csv_transaction.get('description').split())

    if csv_transaction.get('deposit'):
        logger.info("Transaction is a deposit.")
        transaction.amount = csv_transaction.get('deposit')
        transaction.is_debit = False

    elif csv_transaction.get('amount'):
        transaction.amount = csv_transaction.get('amount').strip('-')
        transaction.is_debit = True
    else:
        logger.info(
            "Transaction {} has no amount. Skipping.".format(transaction))
        transaction = None

    return transaction


def is_duplicate_transaction(transaction):
    try:
        duplicate_transaction = Transaction.objects.filter(
            description__iexact=transaction.description,
            date=transaction.date,
            amount=transaction.amount
        )
    except ValidationError as e:
        logger.warning('An error occurred. Skipping transaction. {}'.format(e))
        return True

    if duplicate_transaction:
        logger.warning(
            'Transaction: {} is a duplicate of '
            'Transaction: {}. Not saving transaction.'.format(
                transaction, duplicate_transaction)
        )
        return True

    return False


def set_transaction_category(transaction):
    filtered_transactions = Transaction.objects.filter(
        description=transaction.description)

    if filtered_transactions:
        transaction.category = filtered_transactions.last().category

    return transaction


def import_statement_transactions(filename):
    """
    Import and format transactions from a CSV bank statement.
    There are 3 statement formats: CIBC Visa, Prospera Collabria Visa,
    and Prospera account statement. Check for where statement is from.
    Then deal with fieldnames accordingly
    """
    prospera_fields = ['number', 'date', 'description', 'empty', 'amount',
                       'deposit', 'balance']
    prospera_visa_fields = ['date', 'description', 'category',
                            'reference_number', 'amount']
    cibc_fields = ['date', 'description', 'amount', 'paid', 'card_number']

    if 'download' in filename:
        fieldnames = prospera_visa_fields
    elif 'statement' in filename:
        fieldnames = prospera_fields
    else:
        fieldnames = cibc_fields

    # Open target bank statement csv file
    with open(filename, 'r') as csvfile:
        reader = csv.DictReader(csvfile,  fieldnames=fieldnames)

        for csv_transaction in reader:
            transaction = parse_csv_transaction(csv_transaction)

            # Check for duplicate transaction, if no duplicates then save
            if transaction:
                logger.info("Processing {}".format(transaction))

                if is_duplicate_transaction(transaction):
                    continue
                else:
                    transaction = set_transaction_category(transaction)
                    transaction.save()
