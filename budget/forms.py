from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django.utils import timezone
from django.utils.dates import MONTHS
from budget.models import Transaction, Category

CURRENT_YEAR = timezone.localdate().year
EMPTY_CHOICE = [('', '-------')]

YEAR_CHOICES = [
    (str(year), str(year))
    for year in range(CURRENT_YEAR - 5,  CURRENT_YEAR + 1)
]

MONTH_CHOICES = [
    (key, value) for key, value, in MONTHS.items()
]


class TransactionForm(forms.ModelForm):
    date = forms.DateField()
    amount = forms.DecimalField(decimal_places=2, max_digits=10)
    description = forms.CharField(max_length=200)

    class Meta:
        model = Transaction
        fields = ['date', 'amount', 'description', 'category', 'is_debit']


class IncomeTransactionForm(TransactionForm):
    category = forms.ModelChoiceField(
        queryset=Category.objects.filter(is_debit=False), required=False)


class ExpenseTransactionForm(TransactionForm):
    category = forms.ModelChoiceField(
        queryset=Category.objects.filter(is_debit=True), required=False)


class TransactionFormSetHelper(FormHelper):
    def __init__(self, *args, **kwargs):

        super(TransactionFormSetHelper, self).__init__(*args, **kwargs)
        self.template = 'bootstrap/table_inline_formset.html'
        self.form_method = 'post'
        self.form_class = 'test'
        self.add_input(Submit('submit', 'Submit', css_class='btn-hannah'))


class TransactionFilterForm(forms.Form):
    month = forms.ChoiceField(choices=EMPTY_CHOICE + MONTH_CHOICES)
    year = forms.ChoiceField(choices=EMPTY_CHOICE + YEAR_CHOICES)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['month'].label = False
        self.fields['year'].label = False
        self.fields['year'].initial = CURRENT_YEAR
