#!/usr/bin/env bash
FILES=/home/cameron/Downloads/*.csv

mkdir tmp
echo $PWD/tmp
cp $FILES tmp/

echo "Copying files to remote directory..."
scp -r $PWD/tmp heron:/home/cam/hannah/

echo "Importing transactions to production instance..."
ssh heron "cd hannah/ && ./import_transactions.sh"

echo "Cleaning up. Removing local tmp directory."
rm -rf tmp/
