# Hannah Budget
A little Django budget app

## Prerequisites
```
Python 3.6
Django 
```

## Installing

1. Run the app. You shouldn't actually need a virtualenv as you can simply run the environment via the docker-compose file. 
If developing, run `docker-compose -f docker-compose.yml up --build -d`. 
For production, run `docker-compose -f docker-compose.yml up --build -d`.
1. Create a user. Run `docker-compose exec web python manage.py createsuperuser` and follow the prompts.

## Deployment
Note: The following tutorial was used for the docker-compose setup. https://testdriven.io/blog/dockerizing-django-with-postgres-gunicorn-and-nginx/

### Useful Tools/Articles
* Creating Postgres backups https://axiomq.com/blog/backup-and-restore-a-postgresql-database/

* For restoring Postgres in Docker container https://simkimsia.com/how-to-restore-database-dumps-for-postgres-in-docker-container/

## License
No license yet.